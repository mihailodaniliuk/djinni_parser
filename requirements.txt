aiohttp==3.8.6
beautifulsoup4==4.12.2
lxml==4.8.0
openpyxl==3.1.2
pandas==1.5.0
requests==2.25.1
