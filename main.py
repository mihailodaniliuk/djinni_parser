import asyncio
import time

import aiohttp
import requests
from bs4 import BeautifulSoup
import lxml
import pandas as pd
import os
import openpyxl
from datetime import datetime


async def get_page(session, url):
    while True:
        try:
            async with session.get(url) as r:
                if r.status == 200:
                    return await r.text()
        except Exception as e:
            print(e)


async def get_all(session, urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(get_page(session, url))
        tasks.append(task)
    results = await asyncio.gather(*tasks)
    return results


async def get_all_data_urls(urls, limit=5000):
    timeout = aiohttp.ClientTimeout(total=600)
    connector = aiohttp.TCPConnector(limit=limit)
    async with aiohttp.ClientSession(connector=connector, timeout=timeout) as session:
        data = await get_all(session, urls)
        return data


def get_profile_urls(html):
    urls = []
    soup = BeautifulSoup(html, 'lxml')
    candidate_blocks = soup.find_all('div', class_='card mb-4')
    for candidate_block in candidate_blocks:
        link = candidate_block.find('h2', class_='mt-0 mb-2').find('a').get('href')
        urls.append(f'https://djinni.co{link}&lang=en')
    return urls


def get_all_profile_urls():
    profile_urls = []
    total_pages = get_total_pages()
    links = [f'https://djinni.co/developers/?page={page}&lang=en' for page in range(1, total_pages)]
    full_data = asyncio.run(get_all_data_urls(links, 20))
    for data in full_data:
        profile_urls.extend(get_profile_urls(data))
    return profile_urls


def get_total_pages():
    response = requests.get('https://djinni.co/developers/?page=1&lang=en').text
    soup = BeautifulSoup(response, 'lxml')
    pages_block = soup.find('ul', class_='pagination pagination_with_numbers')
    last_page_block = pages_block.find_all('li')[-2]
    last_page = int(last_page_block.text)
    return last_page


def get_data_from_profile(profile_html):
    soup = BeautifulSoup(profile_html, 'lxml')
    job_title = soup.find('div', class_='detail--title-wrapper').text.strip()
    info_block = soup.find('ul', class_='list-unstyled mb-0')
    lis = info_block.find_all('li')
    salary = soup.find('div', class_='card-header pt-3 pb-3').text.strip().replace(' / mo', '')
    region, exp, english_level, remote_work, relocate, part_time, office, freelance = '', '', '', 'No', 'No', 'No', 'No', 'No'
    skills = ', '.join([skill.text for skill in soup.find_all('span', class_='badge border text-wrap small mb-1')])
    for li in lis:
        if li.find('span', class_='bi bi-globe mr-2 me-2'):
            region = li.text.strip()
        elif li.find('span', class_='bi bi-lightning-charge mr-2 me-2'):
            exp = li.text.strip()
        elif li.find('span', class_='bi bi-chat-text mr-2 me-2'):
            english_level = li.text.replace('English: ', '').strip()
        elif li.text.strip() == 'Remote work':
            remote_work = "Yes"
        elif li.text.strip() == 'Relocate to another country':
            relocate = "Yes"
        elif li.text.strip() == 'Office':
            office = "Yes"
        elif li.text.strip() == 'Freelance (one-time projects)':
            freelance = "Yes"
        elif li.text.strip() == 'Part-time':
            part_time = "Yes"
    return region, job_title, exp, english_level, salary, remote_work, relocate, office, freelance, part_time, skills


if __name__ == '__main__':
    now = datetime.now()
    region_list, job_title_list, exp_list, english_level_list, salary_list, remote_work_list = [], [], [], [], [], []
    relocate_list, office_list, freelance_list, part_time_list, skills_list = [], [], [], [], []
    profile_urls = get_all_profile_urls()
    profile_htmls = asyncio.run(get_all_data_urls(profile_urls))
    for profile_html in profile_htmls:
        profile_data = get_data_from_profile(profile_html)
        region_list.append(profile_data[0])
        job_title_list.append(profile_data[1])
        exp_list.append(profile_data[2])
        english_level_list.append(profile_data[3])
        salary_list.append(profile_data[4])
        remote_work_list.append(profile_data[5])
        relocate_list.append(profile_data[6])
        office_list.append(profile_data[7])
        freelance_list.append(profile_data[8])
        part_time_list.append(profile_data[9])
        skills_list.append(profile_data[10])
    df = pd.DataFrame({'Region': region_list, 'Job title': job_title_list, 'Experience': exp_list,
                       'English level': english_level_list,'Salary':salary_list,
                       'Remote': remote_work_list, 'Relocate': relocate_list, 'Office': office_list,
                       'Freelance': freelance_list,
                       'Part-time': part_time_list, 'Skills': skills_list, 'Profile_url': profile_urls})
    dt_string = now.strftime("%d_%m_%Y %H_%M")
    df.to_excel(f'{dt_string}.xlsx', 'openpyxl', index=False)
